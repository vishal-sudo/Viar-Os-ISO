#!/bin/bash

#arch-chroot "workDir/x86_64/airootfs" /bin/bash -c 'pacman -Qqdt | pacman -Rns - 2>/dev/null'; echo "OUT NOW"

arv(){
arch-chroot "/home/bios/VIAR-OS/iso-folder/workDir/x86_64/airootfs" /bin/bash -c "${1}" ; echo "OUT NOW"
}

arv "$(cat << EOF

echo "Inside Chroot"
pacman -Qqdt | pacman -Rns - 2>/dev/null
cp -rf /usr/share/pixmaps/.neofetch /usr/bin/neofetch
sed -i "s/home/home\/viar/g" /home/viar/.config/xfce4/xfconf/xfce-perchannel-xml/xfce4-panel.xml

EOF
)"
